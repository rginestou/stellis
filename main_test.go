package main

import "testing"

func TestSayHello(t *testing.T) {
	str := SayHello()
	if str != "Hello World!" {
		t.Errorf("Hello World incorrect")
	}
}
