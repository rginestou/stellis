package main

import (
	"log"
)

// SayHello ...
func SayHello() string {
	return "Hello World!"
}

func main() {
	log.Println(SayHello())
}
