all:
	go build -o main main.go

test:
	go test

dep: ## Get the dependencies
	@go get -v -d ./...

.PHONY: all dep